#include <stdio.h>
#include <string.h>

int main()
{
  	char str[1000];
  	int r, length;

  	printf("Enter the Sentence : ");
  	gets(str);
  	length = strlen(str);
  	printf("\nThe sentence after reversing : ");
  	for(r = length - 1; r >= 0; r--)
	{
		if(str[r] == ' ')
		{
			str[r] = '\0';
			printf("%s ", &(str[r]) + 1);
		}
	}
	printf("%s", str);
	printf("\n");
  	return 0;
}
